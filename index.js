function verExperiencia() {
  var element = document.getElementById('experience');
  element.classList.add("active");
  document.getElementById('studies').classList.remove("active");
  document.getElementById('lang').classList.remove("active");
  document.getElementById('others').classList.remove("active");
}

function verEstudios() {
  var element = document.getElementById('studies');
  element.classList.add("active");
  document.getElementById('experience').classList.remove("active");
  document.getElementById('lang').classList.remove("active");
  document.getElementById('others').classList.remove("active");
}

function verIdiomas() {
  var element = document.getElementById('lang');
  element.classList.add("active");
  document.getElementById('experience').classList.remove("active");
  document.getElementById('studies').classList.remove("active");
  document.getElementById('others').classList.remove("active");
}

function verOtros() {
  var element = document.getElementById('others');
  element.classList.add("active");
  document.getElementById('experience').classList.remove("active");
  document.getElementById('studies').classList.remove("active");
  document.getElementById('lang').classList.remove("active");
}